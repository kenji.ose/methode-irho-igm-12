# methode IRHO IGM 12

The method calculates the water balance.
This information makes it possible to intervene usefully in the water supply of 
the palm tree. It is thus possible to assess the quantity of water available to 
the palm tree in relation to its needs.

The script needs the following **directory tree**:
```
input dir  
│
└─── Precipitation : 12 image files of monthly precipitation named from "P1.tif" to "P12.tif"
│
└─── ETP           : 12 image files of monthly ETP named from "ETP1.tif" to "ETP12.tif"
│
└─── RU            : 1 image file of useful reserve named from "RU.tif"
│
└─── results       : empty directory for storing output files
```
To **run the script**, change the path of the variable indir (line 205).
