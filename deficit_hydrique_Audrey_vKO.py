# -*- coding: utf-8 -*-
"""
Created on Wed Mar  2 10:08:15 2022

@author: K. Ose (UMR TETIS - INRAE), A. Jolivot (UMR TETIS - CIRAD)

---
Method: IRHO IGM 12

The method calculates the water balance.
This information makes it possible to intervene usefully in the water supply of 
the palm tree. It is thus possible to assess the quantity of water available to 
the palm tree in relation to its needs.

based on: 
    SURRE C. (1968). Les besoins en eau du palmier à huile : calcul du
    bilan de l'eau et ses applications pratiques. Oléagineux, 23, (3), 165-167
"""

import os
import numpy as np
from osgeo import gdal, osr

def img2array(img_path):
    """
    Convert image file to array

    Parameters
    ----------
    img_path : string
        monoband image file path.

    Returns
    -------
    ar : numpy array
        image in numpy array.

    """
    
    ds = gdal.Open(img_path)
    bd = ds.GetRasterBand(1)
    nd = float(bd.GetNoDataValue())
    ar = bd.ReadAsArray().astype("float32")
    ar[ar == nd] = np.nan
    print('img: ', img_path)
    print('min: {}, max: {}, mean: {}, std: {}'.format(np.nanmin(ar),np.nanmax(ar),
                                                       np.nanmean(ar),np.nanstd(ar)))
    ds = None
    bd = None
    return ar

def extractRef(img_path):
    """
    Extraction of image reference parameters

    Parameters
    ----------
    img_path : string
        reference image file path.

    Returns
    -------
    xsize : float
        spatial resolution x.
    ysize : float
        spatial resolution y.
    prj_wkt : string
        coordinate reference system.
    geotran : tuple
        coefficients for transforming between pixel/line (P,L) raster space, and projection coordinates (Xp,Yp) space.

    """
    ds = gdal.Open(img_path)
    bd = ds.GetRasterBand(1)
    xsize = bd.XSize
    ysize = bd.YSize
    prj_wkt = osr.SpatialReference()
    prj_wkt.ImportFromWkt(ds.GetProjectionRef())
    geotran = ds.GetGeoTransform()
    bd = None
    ds = None
    return (xsize, ysize, prj_wkt, geotran)

def array2img(img_path, array, xsize, ysize, prj_wkt, geotran):
    """
    Convert array to image file

    Parameters
    ----------
    img_path : string
        image file path.
    array : numpy array
        image in numpy array.
    xsize : float
        spatial resolution x.
    ysize : float
        spatial resolution y.
    prj_wkt : string
        coordinate reference system.
    geotran : tuple
        coefficients for transforming between pixel/line (P,L) raster space, and projection coordinates (Xp,Yp) space.

    Returns
    -------
    None.

    """
    driver = gdal.GetDriverByName("GTiff")
    dsOut = driver.Create(img_path, xsize=xsize, ysize=ysize, bands=1, eType=gdal.GDT_Float32)
    dsOut.SetGeoTransform(geotran)
    dsOut.SetProjection(prj_wkt.ExportToWkt())
    bdOut=dsOut.GetRasterBand(1)
    # change Nan values to gdal Nodata = -9999
    array[np.isnan(array)]= -9999
    bdOut.SetNoDataValue(-9999)
    bdOut.WriteArray(array.astype("float32"))
    bdOut.ComputeStatistics(False)
    bdOut = None
    dsOut= None

class Index():
    def __init__(self, RU_arr):
        """
        Index constructor

        Parameters
        ----------
        RU_arr : numpy array
            useful reserve.

        Returns
        -------
        None.

        """
        self.RU = RU_arr
        self.Ri = self.RU
        self.DH_an = 0
        self.mois_sec_tot = 0
        
    def process(self, P_arr, ETP_arr):
        """
        Calculation of water balance (BH), water deficit (DH) and dry month (mois_sec)
        
        # PU: useful precipitation
            PU = P_arr - ETP_arr
            with:
                - P_arr: monthly precipitation
                - ETP_arr: monthly evapotranspiration
        
        # BH: water balance
            BH = PU + Ri
        
        # Ri: soil water reserve at the beginning of month of interest
              (= soil water reserve at the end of the previous month, 
              or useful reserve RU for the first month)
            "Ri = 0"  <-- "BH <= 0"
            "Ri = RU" <-- "BH > 0 and RU < BH"
            "Ri = BH" <-- "BH > 0 and RU > BH"
        
        # DH: water deficit
            "DH = 0"    <-- "BH > 0"
            "DH = |BH|" <-- "BH <= 0"
        
        # mois_sec: dry month
            "mois_sec = 1" <-- "DH > 0"
        
        # DH_an: cumulative water deficit
            DH_an = DH_an + DH 
            (DH_an = 0 for the first month)
                
        # mois_sec_tot: cumulative dry months
            mois_sec_tot = mois_sec_tot + mois_sec 
            (mois_sec_tot = 0 for the first month)
        
        Parameters
        ----------
        P_arr : numpy array
            monthly rain.
        ETP_arr : numpy array
            monthly evapotranspiration.

        Returns
        -------
        BH : numpy array
            water balance for month of interest.
        DH : numpy array
            water deficit for month of interest.

        """
        PU = P_arr - ETP_arr
        #calcul Bilan hydrique
        BH = PU + self.Ri
        self.Ri = (BH <= 0) * 0 + (BH > 0) * ((self.RU < BH) * self.RU + (self.RU >= BH) * BH)
        # calcul deficit hydrique
        DH = (0 > (0 - BH)) * 0 + (0 <= (0 - BH))*(0 - BH)
        mois_sec = (DH > 0) * 1 + 0
        self.DH_an = self.DH_an + DH
        self.mois_sec_tot = self.mois_sec_tot + mois_sec
        return (BH, DH)

if __name__ == "__main__":

    # input files paths
    indir = r"H:\THEOS\test\AWD" 
    path_P = os.path.join(indir, 'Precipitation')
    path_ETP = os.path.join(indir, 'ETP')
    path_RU = os.path.join(indir, 'RU')
    path_result = os.path.join(indir, 'results')
    # output files paths
    RU_img = os.path.join(path_RU, 'RU.tif')
    output_DH_an = os.path.join(path_result, 'DH_an.tif')
    output_mois_sec_tot = os.path.join(path_result, 'Mois_sec_tot.tif')
    # output raster parameters
    ref_xsize, ref_ysize, ref_prj, geotran = extractRef(RU_img)
        
    # list of months begining with the end of the rainy season (i.e. Mexico)
    mois=[10,11,12,1,2,3,4,5,6,7,8,9]

    # RU as Array
    RU_arr = img2array(RU_img)
    # RU conversion (input RU in %  2m deep) in mm 
    RU_arr = RU_arr * 2000 / 100
    
    toto = Index(RU_arr)
    # looping over 12 months
    for a in mois:
        # input file path (month data)
        P_img = os.path.join(path_P, 'P{}.tif'.format(a))
        ETP_img = os.path.join(path_ETP, 'ETP{}.tif'.format(a))
        # output file path (month data)
        output_BH = os.path.join(path_result, 'BH_{}.tif'.format(a))
        output_RF = os.path.join(path_result, 'RF_{}.tif'.format(a))
        output_DH = os.path.join(path_result, 'DH_{}.tif'.format(a))
        # P and ETP as array
        P_arr = img2array(P_img)
        ETP_arr = img2array(ETP_img)    
        # BH and DH processing (per month)
        BH, DH = toto.process(P_arr, ETP_arr)    
        # export monthly BH and DH as images
        array2img(output_BH, BH, ref_xsize, ref_ysize, ref_prj, geotran)
        array2img(output_DH, DH, ref_xsize, ref_ysize, ref_prj, geotran)
    # get array of DH_an and mois_sec_tot
    DH_an = toto.DH_an
    mois_sec_tot = toto.mois_sec_tot
    # export annual DH and total dry months as images    
    array2img(output_DH_an, DH_an, ref_xsize, ref_ysize, ref_prj, geotran)
    array2img(output_mois_sec_tot, mois_sec_tot, ref_xsize, ref_ysize, ref_prj, geotran)

    print("End of process")
